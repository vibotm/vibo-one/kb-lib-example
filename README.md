# ViBo kb-lib example

Simple usage example of the [ViBo kb-lib](https://gitlab.com/vibotm/vibo-one/kb-lib) ESP-IDF component.

## Dependencies

1. [ESP-IDF v5.2.1](https://github.com/espressif/esp-idf/releases)
2. [ViBo kb-lib v1.2.0](https://gitlab.com/vibotm/vibo-one/kb-lib/-/releases/v.1.2.0)

[ESP-IDF SDK setup](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/get-started/index.html)

## Example info

Example uses a 2x2 key keyboard matrix with a quadrature rotary encoder.

The following circuit was used to test the example:

![example schematic](doc/schematic.png)

## Supported targets

Any ESP-IDF supported target (list supported ESP-IDF targets using `idf.py --list-targets`).

## Building the example

1. Clone example with its respective components (use the `--recursive` option while cloning)
2. Build example using either `cmake` and `ninja` or ESP-IDFs `idf.py` meta tool

## Build using cmake and ninja

```
mkdir build
cd build
cmake .. -GNinja
ninja
```

## Build using idf.py
```
idf.py build
```

## Run example

Flash the example to your target using `ninja flash` or `idf.py flash`.
