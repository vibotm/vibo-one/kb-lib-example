#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include "kb-lib.h"

//keyboard matrix config
#define     ROWS                        2
#define     COLLUMNS                    2

kb_keymap_t keymap[ROWS][COLLUMNS];
uint8_t     rowPinmap[ROWS]         = { 22, 23 };
uint8_t     collumnPinmap[COLLUMNS] = { 18, 19 };
keyboard_t  keyboard;

//rotary encoder config
#define     EC_PIN_A                    25
#define     EC_PIN_B                    26
#define     EC_PIN_KEY                  27

kb_keymap_t encoderKey;
encoder_t   encoder;

void app_main(void) {
    kb_err_t ret;
    
    //init keyboard matrix
    //create empty keymap matrix
    for (uint8_t row = 0; row < ROWS; row++) {
        for (uint8_t collumn = 0; collumn < COLLUMNS; collumn++) {
            ret = createKeymap(0, NULL, 0, &keymap[row][collumn]);
            if (ret != KB_OK) {
                printf("Failed to create keymap!\n");
                return;
            }
        }//for (uint8_t collumn...
    }//for (uint8_t row...
    
    //initialise keyboard matrix
    ret = keyboardInit(&keyboard, &keymap[0][0], ROWS, COLLUMNS, &rowPinmap[0], &collumnPinmap[0]);
    if (ret != KB_OK) {
        printf("Failed to initialise keyboard matrix!\n");
        return;
    } 
    
    //rotary encoder init
    //create empty keymap for encodedr key
    createKeymap(0, NULL, 0, &encoderKey);

    //initialise encoder
    ret = encoderInit(&encoder, &encoderKey, EC_PIN_A, EC_PIN_B, EC_PIN_KEY);
    if (ret != KB_OK) {
        printf("Failed to initialise rotary encoder!\n");
        return;
    }
    
    //start kb-lib event handling loops
    ret = keyboardStart(&keyboard);
    if (ret != KB_OK) {
        printf("Failed to start keyboard loop!\n");
        return;
    }
    ret = encoderStart(&encoder);
    if (ret != KB_OK) {
        printf("Failed to start encoder loop!\n");
        return;
    }
    
    //handle kb-lib events
    while (true) {
        //handle keyboard matrix event if available
        bool eventAvailable = false;
        keyboardEventAvailable(&keyboard, &eventAvailable);

        if (eventAvailable) {
            kb_event_t kbEvent;
            keyboardGetEvent(&keyboard, &kbEvent);
            printf("key [%d, %d] event: %d\n", kbEvent.row, kbEvent.collumn, kbEvent.state);
        }
        
        //alternative event handling demonstrated for rotary encoder events
        ec_event_t event = {
            .state = KEY_EMPTY_STATE,
        };
        encoderGetEvent(&encoder, &event);

        switch (event.state) {
            case EC_POSITION_CHANGED:
                printf("ec position: %" PRIi32 "\n", event.position);
                break;

            case KEY_JUST_PRESSED:
                printf("ec key pressed!\n");
                break;

            case KEY_JUST_RELEASED:
                printf("ec key released!\n");
                break;

            default:
                break;
        }//switch (event.state)
    }//while (true)
}//app_main
